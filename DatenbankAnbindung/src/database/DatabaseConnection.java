package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;



public class DatabaseConnection {

	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/kekse";
	private String user = "root";
	private String password = "";
	
	public boolean insertKekse(String bezeichnung, String sorte) {
		String sql ="INSERT INTO kekse_t_kekse (bezeichnung, sorte) VALUES ('"+ bezeichnung +"', '"+ sorte +"')";
		
		
		//JDBC-Treiber Laden
		try {
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			Statement stmt = con.createStatement();
			stmt.executeUpdate(sql);
			
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		DatabaseConnection dbc = new DatabaseConnection();
		dbc.insertKekse("---", "---");
	}
}
