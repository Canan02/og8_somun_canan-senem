/* Fibonacci.java
   Programm zum Testen der rekursiven und der iterativen Funktion
   zum Berechnen der Fibonacci-Zahlen.
   AUFGABE: Implementieren Sie die Methoden fiboRekursiv() und fiboIterativ()
   HINWEIS: siehe Informationsblatt "Fibonacci-Zahlen oder das Kaninchenproblem"
   Autor:
   Version: 1.0
   Datum:
*/
public class Fibonacci{
   // Konstruktor
   Fibonacci(){
   }
   
  /**
    * Rekursive Berechnung der Fibonacci-Zahl an n-te Stelle
    * @param n 
    * @return die n-te Fibonacci-Zahl
   */
  long fiboRekursiv(int n){
	  if(n <=1)
		  return n;
	  else
		  return fiboRekursiv(n-1) + fiboRekursiv(n-2);
  }//fiboRekursiv
  

  /**
   * Iterative Berechnung der Fibonacci-Zahl an n-te Stelle
   * @param n 
   * @return die n-te Fibonacci-Zahl
  */
  long fiboIterativ(int n){
	  long vorletztes = 0, letztes = 1, jetziges =0;
	     if(n <= 1)
	       return n;
	     else {
	       for(int i=2; i<=n; i++){
	         jetziges = letztes + vorletztes;
	         vorletztes = letztes;
	         letztes = jetziges;
	       }
	       return jetziges;
	     }
	  }//fiboIterativ

}// Fibonnaci


