package hausaufgabe05_11;

public class Stoppuhr {
	private static long startzeit;
	private static long endzeit;
	
	public static void start() {
		startzeit  = System.currentTimeMillis();
	}
	
	public static void stopp() {
		endzeit = System.currentTimeMillis();
	}
	
	public static long getDauer_Ms() {
		return endzeit - startzeit;
	}

}
