package hausaufgabe05_11;

import java.util.Arrays;

public class BinaerSuche {

    public static void main(String[] args) {

        System.out.println("Start");
        
        long[] longArray = new long[100000];
        long zahl = 0;
        long counterBinarySearch = 0;

        for (int i = 0; i < 1000; i++) {


            for (int i1 = 0; i1 < 100000; i1++) {
                longArray[i1] = (long) (Math.random() * 15_000_000);
            }
            Arrays.sort(longArray);
            zahl = longArray[(int) Math.random() * 100000];

            System.out.println( + i + ".");
            counterBinarySearch += binarySearch(longArray, zahl);
        }
        System.out.println("Im Schnitt (1000 Versuche) wurden " + counterBinarySearch/1000 + " Durchläufe gebraucht.");
    }

    public static long binarySearch(long[] longArray, long zahl) {
        long n = 0;
        int i = longArray.length / 2;
        while (zahl != longArray[i]) {
            n++;
            if (zahl < longArray[i]) {
                i = i / 2;
            } else {
                i += i / 2;
            }
            if (n > longArray.length / 2) {
                return -1;
            }
        }
        return n;
    }
}