package hausaufgabe05_11;

import java.util.Random;

public class ListenGenerator {
	static Random rand = new Random();
    public static long[] getSortedList(int laenge) {
        
        long[] zahlenliste = new long[laenge];
        long naechsteZahl = 0;

        for (int i = 0; i < laenge; i++) {
            naechsteZahl += rand.nextInt(3) + 1;
            zahlenliste[i] = naechsteZahl;
        }

        return zahlenliste;
    }

    public static void main(String[] args) {
        final int ANZAHL = 20_000_000; //Von 15.000.000 bis 20.000.000. In 1 Million Schritte

        //worst-case
        long[] a = getSortedList(ANZAHL);
        long gesuchteZahl = a[ANZAHL - 1];

        Stoppuhr.start();
        binarySearch(a, gesuchteZahl);
        Stoppuhr.stopp();
        System.out.println("F�r BinarySearch im 'worst-case' wurden " + Stoppuhr.getDauer_Ms() *1000 + " Mikrosekunden gebraucht.");

        Stoppuhr.start();
        linearSearch(a, gesuchteZahl);
        Stoppuhr.stopp();
        System.out.println("F�r LinearSearch im 'worst-case' wurden " + Stoppuhr.getDauer_Ms() *1000 + " Mikrosekunden gebraucht.");
        
        //Average-case
        gesuchteZahl = a[rand.nextInt(5000000) + 10000000 -1];
        Stoppuhr.start();
        binarySearch(a, gesuchteZahl);
        Stoppuhr.stopp();
        System.out.println("F�r BinarySearch im 'average-case' wurden " + Stoppuhr.getDauer_Ms() *1000 + " Mikrosekunden gebraucht.");
        
        gesuchteZahl = a[rand.nextInt(5000000) + 10000000 -1];
        Stoppuhr.start();
        linearSearch(a, gesuchteZahl);
        Stoppuhr.stopp();
        System.out.println("F�r LinearSearch im 'average-case' wurden " + Stoppuhr.getDauer_Ms() *1000 + " Mikrosekunden gebraucht.");
        
        //Best-Case
        gesuchteZahl = a[ANZAHL /2];
        Stoppuhr.start();
        binarySearch(a, gesuchteZahl);
        Stoppuhr.stopp();
        System.out.println("F�r BinarySearch im 'best-case' wurden " + Stoppuhr.getDauer_Ms() *1000 + " Mikrosekunden gebraucht.");
        
        gesuchteZahl = a[ANZAHL /2];
        Stoppuhr.start();
        linearSearch(a, gesuchteZahl);
        Stoppuhr.stopp();
        System.out.println("F�r LinearSearch im 'best-case' wurden " + Stoppuhr.getDauer_Ms() *1000 + " Mikrosekunden gebraucht.");
    }
    

    
    	

    public static long binarySearch(long[] longArray, long zahl) {
        long n = 0;
        int grenze = longArray.length;
        int lengthOne = longArray.length / 2;
        while (zahl != longArray[lengthOne]) {


            if (zahl < longArray[lengthOne]) {
                grenze = lengthOne;
                lengthOne = lengthOne / 2;

            } else {
                lengthOne += (grenze - lengthOne) / 2;
            }

            if (n > longArray.length) {
                return -1;
            }
            n++;
        }
        return n + 1;
    }

    public static long linearSearch(long[] longArray, long zahl) {
        for (int lengthTwo = 0; lengthTwo < longArray.length; lengthTwo++) {
            if (Long.compare(longArray[lengthTwo], zahl) == 0) {
                return lengthTwo + 1;
            }
        }
        return -1;
    }
}



