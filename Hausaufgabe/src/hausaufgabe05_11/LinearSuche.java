package hausaufgabe05_11;

import java.util.Arrays;

public class LinearSuche{

    public static void main(String[] args) {

        System.out.println("Start");
        Stoppuhr.start();
        Stoppuhr.stopp();
        System.out.println(Stoppuhr.getDauer_Ms()+ "ms");
        long[] longArray = new long[100000];
        long zahl = 0;
        long counterBinarySearch = 0;
        long counterLinearSearch = 0;

        for (int c = 0; c < 1000; c++) {


            for (int i = 0; i < 100000; i++) {
                longArray[i] = (long) (Math.random() * 1000000000);
            }
            Arrays.sort(longArray);
            zahl = longArray[(int) (Math.random() * 100000)];

            System.out.println( + c + ".");
            counterBinarySearch += binarySearch(longArray, zahl);
            counterLinearSearch += linearSearch(longArray, zahl);
        }
        System.out.println("Im Schnitt (1000 Versuche) wurden f�r BinarySearch " + counterBinarySearch / 1000 + " Durchl�ufe gebraucht.");
        System.out.println("Im Schnitt (1000 Versuche) wurden f�r LinearSearch " + counterLinearSearch / 1000 + " Durchl�ufe gebraucht.");
    }

    public static long binarySearch(long[] longArray, long zahl) {
        long n = 0;
        int grenze = longArray.length;
        int lengthOne = longArray.length / 2;
        while (zahl != longArray[lengthOne]) {


            if (zahl < longArray[lengthOne]) {
                grenze = lengthOne;
                lengthOne = lengthOne / 2;

            }
            else {
                lengthOne += (grenze - lengthOne) / 2;
            }

            if (n > longArray.length) {
                return -1;
            }
            n++;
        }
        return n+1;
    }

    public static long linearSearch(long[] longArray, long zahl) {
        for (int lengthTwo = 0; lengthTwo < longArray.length; lengthTwo++) {
            if (Long.compare(longArray[lengthTwo], zahl) == 0) {
                return lengthTwo + 1;
            }
        }
        return -1; 
    }
}
