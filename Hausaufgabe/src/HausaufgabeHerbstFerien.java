public class HausaufgabeHerbstFerien {
	public static void main(String[] args) {
		// Grenze bis zu der Primzahlen gesucht werden.
		int n = 21212;
		// beginne bei der Zahl 2 da 1 keine Primzahl ist.
		for (int i = 2; i <= n; i++) {
			// isPrimzahl ist ein Wahrheitswert, um zu ueberpruefen ob wir eine
			// Primzahl gefunden haben. Falls nicht, wird sie spaeter auf false
			// gesetzt.
			boolean isPrimzahl = true;
			// i durch jede Zahl kleiner als i zu teilen und zu checken, ob
			// ein Rest uebrig bleibt. Zum Beispiel: Im Wiki 25=i und j waere die Variable
			// die hochgezaehlt wird.
			for (int j = 2; j < i == isPrimzahl; j++) {
				// Wenn eine division als rest 0 hat dann ist die Zahl keine
				// Primzahl und somit ist isPrimzahl auf false zu setzten
				if ((i % j) == 0) {
					isPrimzahl = false;
				}
			}
			// Gib eine Meldung aus, falls es sich um eine Primzahl handelt.
			if (isPrimzahl) {
				System.out.println(i + " ist eine Primzahl! :)");
			}else {
				System.out.println(i + " ist keine Primzahl du Dulli -.-");
			}
		}
	}
}