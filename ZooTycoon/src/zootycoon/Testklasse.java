package zootycoon;

public class Addon {
	private int idnummer;
	private String bezeichnung;
	private double verkaufspreis;
	private int bestandIst;
	private int bestandmax;

	public Addon() {

	}

	public int getidnummer() {
		return idnummer;
	}

	public String getbezeichnung() {
		return bezeichnung;
	}

	public double getverkaufspreis() {
		return verkaufspreis;
	}

	public int getbestandIst() {
		return bestandIst;
	}

	public int getbestandmax() {
		return bestandmax;
	}

	public void setidnummer(int idnummer) {

		this.idnummer = idnummer;
	}

	public void setbezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}

	public void setverkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public void setbestandIst(int bestandIst) {
		this.bestandIst = bestandIst;
	}

	public void  setbestandmax (int bestandmax) {
		this.bestandmax = bestandmax;
	}
	
	public double berechnegesamtwert() {
		return this.verkaufspreis * this.bestandIst;
	}
	
	public void  aenderebestand (int anzahl) {
		
		 
		 bestandIst = bestandIst + anzahl;
	}
}





	
	
