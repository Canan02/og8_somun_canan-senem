package schleifen;

import java.util.Scanner;

public class schleifenTest {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int anzahl;
		System.out.println("Bitte geben Sie eine Zahl ein: ");
		anzahl = scan.nextInt();

		// pluszeichen Generieren
		for (int i = 0; i < anzahl; i++) {
			System.out.print("+");
		}
		//Minuszeichen Generieren
		int i = 0;
		while( i < anzahl) {
			System.out.print("-");
			i++;
		}
		//Malzeichen
		i = 0;
		do {
			System.out.print("*");
			i++;
		}while(i < anzahl);
		}

}


