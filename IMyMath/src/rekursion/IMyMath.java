package rekursion;

public interface IMyMath {
	
public static void main(String[] args) {
	for (int i = 0; i<= 100; i++)
		System.out.println(i+"Zahl\t"+fiboRekursiv(i)+"\t"+fiboIterativ(i));

}
	  static long fiboRekursiv(int n){
		    if(n <= 1)
		      return n;
		    else
		      return fiboRekursiv(n-1) + fiboRekursiv(n-2);
		  }


		  static long fiboIterativ(int n){
		     long vorletztes = 0, letztes = 1, jetziges =0;
		     if(n <= 1)
		       return n;
		     else {
		       for(int i=2; i<=n; i++){
		         jetziges = letztes + vorletztes;
		         vorletztes = letztes;
		         letztes = jetziges;
		       }//for
		       return jetziges;
		     } //else
		  }
}
