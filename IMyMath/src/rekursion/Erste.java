package rekursion;

public interface Erste {
	
	int fakultaet(int zahl);
	
	int fibonacci(int zahl);
}