package JUnit;

import static org.junit.Assert.*;

import org.junit.Test;

import Data.kgV;
import Work.DecimalToRoman;

public class Test_kgV {

  @Test
  public void test1_18() {
    assertEquals(18, kgV.kgv(1, 18));
  }
  @Test
  public void testminus1_18() {
    assertEquals(-1, kgV.kgv(-1, 18));
  }
  @Test
  public void test7_14() {
    assertEquals(14, kgV.kgv(7, 14));
  }
  @Test
  public void testminus5_16() {
    assertEquals(-1, kgV.kgv(-5, 16));
  }
  @Test
  public void test74_18() {
    assertEquals(666, kgV.kgv(74, 18));
  }
  @Test
  public void test2_3() {
    assertEquals(6, kgV.kgv(2, 3));
  }
  @Test
  public void test24_7() {
    assertEquals(168, kgV.kgv(24, 7));
  }

  
}