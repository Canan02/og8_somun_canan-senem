package JUnit;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import Work.DecimalToRoman;

class TestKGV {
	@Test
	  public void testAusserhalb1() {
	    assertEquals("Außerhalb des zulässigen Bereichs", DecimalToRoman.dezimalToRoman(-1));
	  }

	  @Test
	  public void testAusserhalb2() {
	    assertEquals("Außerhalb des zulässigen Bereichs", DecimalToRoman.dezimalToRoman(0));
	  }

	  @Test
	  public void testAusserhalb3() {
	    assertEquals("Außerhalb des zulässigen Bereichs", DecimalToRoman.dezimalToRoman(3500));
	  }

	  @Test
	  public void testI() {
	    assertEquals("I", DecimalToRoman.dezimalToRoman(1));
	  }

	  @Test
	  public void testIV() {
	    assertEquals("IV", DecimalToRoman.dezimalToRoman(4));
	  }

	  @Test
	  public void testV() {
	    assertEquals("V", DecimalToRoman.dezimalToRoman(5));
	  }

	  @Test
	  public void testIX() {
	    assertEquals("IX", DecimalToRoman.dezimalToRoman(9));
	  }

	  @Test
	  public void testX() {
	    assertEquals("X", DecimalToRoman.dezimalToRoman(10));
	  }

	  @Test
	  public void testXI() {
	    assertEquals("XI", DecimalToRoman.dezimalToRoman(11));
	  }

	  @Test
	  public void testXVIII() {
	    assertEquals("XVIII", DecimalToRoman.dezimalToRoman(18));
	  }

	  @Test
	  public void testXXXIX() {
	    assertEquals("XXXIX", DecimalToRoman.dezimalToRoman(39));
	  }

	  @Test
	  public void testXLIX() {
	    assertEquals("XLIX", DecimalToRoman.dezimalToRoman(49));
	  }

	  @Test
	  public void testL() {
	    assertEquals("L", DecimalToRoman.dezimalToRoman(50));
	  }

	  @Test
	  public void testLX() {
	    assertEquals("LX", DecimalToRoman.dezimalToRoman(60));
	  }

	  @Test
	  public void testC() {
	    assertEquals("C", DecimalToRoman.dezimalToRoman(100));
	  }

	  @Test
	  public void testCD() {
	    assertEquals("CD", DecimalToRoman.dezimalToRoman(400));
	  }

	  @Test
	  public void testD() {
	    assertEquals("D", DecimalToRoman.dezimalToRoman(500));
	  }

	  @Test
	  public void testDC() {
	    assertEquals("DC", DecimalToRoman.dezimalToRoman(600));
	  }

	  @Test
	  public void testMMDCCCLXXXVIII() {
	    assertEquals("MMDCCCLXXXVIII", DecimalToRoman.dezimalToRoman(2888));
	  }

	  @Test
	  public void testMMXV() {
	    assertEquals("MMXV", DecimalToRoman.dezimalToRoman(2015));
	  }

	  @Test
	  public void testMMMCD() {
	    assertEquals("MMMCD", DecimalToRoman.dezimalToRoman(3400));
	  }


	}


