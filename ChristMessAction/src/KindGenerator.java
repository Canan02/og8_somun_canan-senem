import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;

public class KindGenerator {

	// Hier eine Methode erstellen, um die Datei der Kinder auszulesen und mit den
	// Daten eine Liste an Kind-Objekte anlegen

	public static Kind[] auslesen() {
		Kind kd = new Kind();

		Kind arr[] = new Kind[10000];
		File file = new File("kinddaten.txt");

		BufferedReader br = null;

		try {
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while ((line = br.readLine()) != null) {
				line = line.replace(",", "");
				String[] erg = line.split(" ");
				kd.setVornamen(erg[0]);
				kd.setNachnamen(erg[1]);
				kd.setGeburtstag(erg[2]);
				kd.setBravheitsgrad(Integer.valueOf(erg[3]));
				String ort = "";
				for (int i = 4; i < erg.length; i++)
					ort += erg[i] + " ";
				kd.setWohnort(ort);

				System.out.println("Kind:" + " " + kd.getVornamen() + " "+ kd.getNachnamen() + " "+ kd.getGeburtstag()
						+ " " +kd.getBravheitsgrad() + " " +kd.getWohnort());

			}

		} catch (FileNotFoundException e) {
			System.out.println("File not found: " + file.toString());
		} catch (IOException e) {
			System.out.println("Unable to read file: " + file.toString());
		}
		return null;
	}
}