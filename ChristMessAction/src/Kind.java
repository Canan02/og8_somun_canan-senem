import java.util.*;

public class Kind{
	
	
	private String Nachnamen, Vornamen, Geburtstag, Wohnort  ;
	private int Bravheitsgrad;
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	//Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode
	public String getNachnamen() {
		return Nachnamen;
	}
	public void setNachnamen(String nachnamen) {
		Nachnamen = nachnamen;
	}
	public String getVornamen() {
		return Vornamen;
	}
	public void setVornamen(String vornamen) {
		Vornamen = vornamen;
	}
	public String getGeburtstag() {
		return Geburtstag;
	}
	public void setGeburtstag(String geburtstag) {
		Geburtstag = geburtstag;
	}
	public String getWohnort() {
		return Wohnort;
	}
	public void setWohnort(String wohnort) {
		Wohnort = wohnort;
	}
	public int getBravheitsgrad() {
		return Bravheitsgrad;
	}
	public void setBravheitsgrad(int bravheitsgrad) {
		Bravheitsgrad = bravheitsgrad;
	}
	
	
	public int compareTo(Object o) {
		Kind other = (Kind) o;
		return new Integer(this.Bravheitsgrad).compareTo (other.Bravheitsgrad);
	}
}

