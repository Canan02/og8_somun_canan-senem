package omnom;

public class Haustier {
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	private String name;




public Haustier () {
	
	
}
	
	public Haustier (String name) {
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
		
	
	
}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		if(hunger >100)hunger=100;
		if(hunger<0)hunger=0;
		this.hunger = hunger;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		if(muede>100)hunger=100;
		if(muede<0)muede=0;
		this.muede = muede;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		if(zufrieden>100)zufrieden=100;
		if(zufrieden<0)zufrieden=0;
		this.zufrieden = zufrieden;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		if(gesund>100)gesund=100;
		if(gesund<0)gesund=0;
		this.gesund = gesund;
	}

	public void fuettern (int anzahl) {
		setHunger(hunger+anzahl);
	}
	public void schlafen (int dauer) {
		setMuede(muede+dauer);
	}
	public void spielen(int dauer) {
		setZufrieden(zufrieden + dauer);
	
	}
	public void heilen() {
	gesund=100;
	}
	
}